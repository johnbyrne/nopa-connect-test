import React from 'react';
import { Layout } from '../../components';
import { Link } from "react-router";

const HomePage = (props) => {
  return (
    <Layout title="Welcome to Nopa!">
      <div className="home-page">
	      <h2>Your finances, in one place</h2>
	      <small>
	        Track all of your payments by connecting as many bank accounts as you'd like to your Nopa<br />
	        account and get updates from your balance instantly. Plus it's free.
	      </small><br />
        <Link to="/login">
		      <button className="start-button button bg-red">Get started</button>
		    </Link>
	      <div className="home-info bg-white">
	        <div className="details">
	          <h2 className="color-blue">There's no such thing as "one size" fits all finance.</h2>
	          <small className="color-black">We were founded to make money simple and fair, for everyone: whether you're looking for a loan, or to get better rewards for your investment.</small>	
	        </div>
	        <div className="image">
			      <img className="header-logo" src={require('../../../static/images/Shapes.png')} />
	        </div>
	      </div>
      </div>
    </Layout>
  );
}

export default HomePage;
