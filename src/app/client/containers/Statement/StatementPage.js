import React, { Component, PropTypes } from 'react'
import { Layout } from '../../components';
import { connect } from 'react-redux'
import { getStatement } from '../../redux/actions/actions'
import StatementList from '../../components/StatementList/StatementList'

class StatementPage extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getStatement()
    this.forceUpdate()
  }

  render() {
    const { statement } = this.props

    return (
      <Layout title="Statement">
	      <div className="statement-page">
	      	<h2>Statement</h2>
		      <small>
		        Track all of your payments by connecting as many bank accounts as you'd like to your Nopa<br />
		        account and get updates from your balance instantly. Plus it's free.
		      </small><br />
		      <small>Your transactions for the last 30 days</small>
	        {statement != undefined &&
	          <StatementList data={statement} />
	        }
	      </div>
      </Layout>
    )
  }
}

StatementPage.propTypes = {
  statement: PropTypes.array.isRequired,
  getStatement: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {

  const { bankDataState } = state

  const {
    statement
  } = bankDataState || {
    statement: []
  }
  return {
    statement
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getStatement: () => {
      dispatch(getStatement())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatementPage)
