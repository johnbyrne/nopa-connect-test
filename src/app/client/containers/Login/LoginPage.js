import React, { Component, PropTypes } from 'react'
import { Layout } from '../../components'
import { browserHistory } from 'react-router'

class LoginPage extends Component {
  constructor(props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  checkSortcode(string) {
  	return /^(?!(?:0{6}|00-00-00))(?:\d{6}|\d\d-\d\d-\d\d)$/.test(string)
  }

  checkNumeric(string) {
  	return (!isNaN(parseFloat(string)) && isFinite(string))
  }

  checkString(string) {
  	return (string.indexOf(string) >= 0)
  }

  onClick(event) {
  	let surname = event.target.parentElement.elements[0].value,
  	  sortcode = event.target.parentElement.elements[1].value,
  	  accountNumber = event.target.parentElement.elements[2].value,
  	  memorableWord = event.target.parentElement.elements[4].value;
  	  
  	if (
  		event !== undefined &&
  		this.checkString(surname) && this.checkSortcode(sortcode) &&
  		this.checkNumeric(accountNumber) && this.checkString(memorableWord)
  	) {
			browserHistory.push('/choose-bank')
  	}
  }

  render() {
    const { onClick } = this

    return (
			<Layout title="Login page">
		      <div className="login-page">
			      <h2>Which bank does this account belong to?</h2>
			      <small>
			        Track all of your payments by connecting as many bank accounts as you'd like to your Nopa<br />
			        account and get updates from your balance instantly. Plus it's free.
			      </small><br />
			      <form className="login-form">
				      <input placeholder="Surname" /><br />
				      <input placeholder="Sort code" /><br />
				      <input placeholder="Account number" /><br />
				      <input placeholder="Passcode" /><br />
				      <input placeholder="Memorable word" /><br />
				      <button type="submit" onClick={this.onClick} className="button bg-red">Log in & connect</button>
			      </form>
		      </div>
		    </Layout>
    )
  }
}

export default LoginPage;
