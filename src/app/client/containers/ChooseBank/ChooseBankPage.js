import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Layout } from '../../components';
import { Link } from "react-router";
import { proceedToStatement, setSelectedBank, fetchBankData } from '../../redux/actions/actions'

class ChooseBankPage extends Component {

  constructor(props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  onClick() {
    //this.props.setSelectedBank(this.props.bankData[index])
    this.props.fetchBankData()
    this.props.proceedToStatement()
  }

  render() {
    const { bankData } = this.props
    const { onClick } = this

    return (
			<Layout title="Choose bank">
	      <div className="choose-bank-page">
		      <h2>Which bank does this account belong to?</h2>
		      <small>
		        Track all of your payments by connecting as many bank accounts as you'd like to your Nopa<br />
		        account and get updates from your balance instantly. Plus it's free.
		      </small><br />
		      <div className="bank-selection-grid">
		      	<div className="row">
		      		<div>&nbsp;</div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-barclays.png')} /></div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-natwest.png')} /></div>
		      		<div>&nbsp;</div>
		      	</div>
		      	<div className="row">
		      		<div>&nbsp;</div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-lloyds.png')} /></div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-hsbc.png')} /></div>
		      		<div>&nbsp;</div>
		      	</div>
		      	<div className="row">
		      		<div>&nbsp;</div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-tsb.png')} /></div>
		      		<div><img className="bank-logo" src={require('../../../static/images/bank-santander.png')} /></div>
		      		<div>&nbsp;</div>
		      	</div>
		      </div>
			    <button onClick={ this.onClick } className="connect-button button bg-red">Connect</button>
	      </div>
	    </Layout>
    )
  }
}

ChooseBankPage.propTypes = {
  setSelectedBank: PropTypes.func.isRequired,
  proceedToStatement: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {

  const { bankDataState  } = state

  const {
    isLoading
  } = bankDataState || {
    isLoading: true,
    bankData: []
  }

  return {
    isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBankData: (inputText) => {
      dispatch(fetchBankData(inputText))
    },
    setSelectedBank: (bankObject) => {
      dispatch(setSelectedBank(bankObject))
    },
    proceedToStatement: () => {
      dispatch(proceedToStatement())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChooseBankPage)

