import React from 'react'
import { Link } from "react-router";

const Header = (props) => {
  return (
    <div className="header-content">
      <img className="header-logo" src={require('../../../static/images/logo-nopa.png')} />
		  <Link to="/login">
			  <button className="header-login button bg-red">Log in</button>
		  </Link>
    </div>
  )
}

export default Header
