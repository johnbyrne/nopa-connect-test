import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

class StatementList extends Component {
  constructor(props) {
    super(props)
  }

  groupData(data) {
    let groups = {}
    for (let value of data) {
      if (groups[value.dateStr] === undefined) {
        groups[value.dateStr] = [value]
      } else {
        groups[value.dateStr].push(value)
      }
    }
    return groups
  }

  generateRows(data) {
    let groupedData = this.groupData(data)
    let datesAdded = []
    return Object.keys(groupedData).map((key, j) => {
      let statementItemData = groupedData[key].map((item, i) => {
        return (
          <div className="statement-item">
            <div className="beneficary">{groupedData[key][i].beneficary}</div>
            <div className="quantity">{groupedData[key][i].value}</div>
          </div>
        )
      });

      return (
        <div className="statement-item-date-group">
          <div className="statement-item-title">{key === 'Now' ? 'Today' : key}</div>
          {statementItemData}
        </div>
      )
    })
  }

  render() {
    const { statementData } = this.props.data

    let rowComponents = this.generateRows(this.props.data)

    return (
      <div>
        <div className="statement-list-container">
          {rowComponents}
        </div>
      </div>
    )
  }
}

StatementList.propTypes = {
  data: PropTypes.array.isRequired
}

export default StatementList