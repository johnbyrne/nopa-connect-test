import React from 'react'

const Footer = (props) => {
  return (
    <div className="footer-content">
      <div className="partners bg-lightblue">
        <div className="partners-label">Our partners:</div>
        <div className="partners-logo"><img src={require('../../../static/images/logo-airbnb.png')} /></div>
        <div className="partners-logo"><img src={require('../../../static/images/logo-metro.png')}  /></div>
        <div className="partners-logo"><img src={require('../../../static/images/logo-pariti.png')}  /></div>
        <div className="partners-logo"><img src={require('../../../static/images/logo-unshackled.png')}  /></div>
      </div>
      <div className="copyright bg-darkblue">
        <small>
      		<div className="copyright-section">© Zopa Limited 2017 All rights reserved. 'Zopa' and the Zopa logo are trade marks of Zopa Limited. Zopa is a member of CIFAS – the UK's leading anti-fraud association, and we are registered with the Office of the Information Commissioner (No. Z879078).</div><br />
      		<div className="copyright-section">Zopa Limited is incorporated in England & Wales (registration number 05197592), with its registered office at 1st Floor, Cottons Centre, Tooley Street, London, SE1 2QG. Zopa Limited is authorised and regulated by the Financial Conduct Authority, and entered on the Financial Services Register under firm registration number 563134.</div>
        </small>
      </div>
    </div>
  )
}

export default Footer
