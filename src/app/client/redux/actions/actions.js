import { history, push } from 'react-router-redux'
import fetch from 'isomorphic-fetch'
import { browserHistory } from 'react-router'

// Async actions

import {
  REQUEST_STATEMENT_DATA,
  RECEIVE_STATEMENT_DATA } from './actionTypes'

const requestBankData = (inputText) => {
  return {
    type: REQUEST_STATEMENT_DATA,
    inputText
  }
}

const receiveBankData = (json) => {
  return {
    type: RECEIVE_STATEMENT_DATA,
    statement: json.transactions
  }
}

export const fetchBankData = (inputText) => {
  return (dispatch) => {
    dispatch(requestBankData(inputText))
    return fetch(`api/transactions/`)
      .then((response) => response.json())
      .then((json) => {
        dispatch(receiveBankData(json))
      })
  }
}

// Route changes

export const proceedToStatement = (statementData) => {
  return (dispatch, getState) => {
    const { transactions } = getState()
    browserHistory.push('/statement')
  }
}

// Page actions

import {
  GET_STATEMENT } from './actionTypes'

export const getStatement = () => {
  return {
    type: GET_STATEMENT
  }
}

