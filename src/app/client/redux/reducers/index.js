import {combineReducers} from 'redux';
import bankReducer from './bankReducer';

import {
  REQUEST_STATEMENT_DATA,
  RECEIVE_STATEMENT_DATA } from '../actions/actionTypes'

// Asynchronous/services state

const bankDataState = (state = {
  isLoading: false,
  statement: []
}, action) => {
  switch (action.type) {
    case REQUEST_STATEMENT_DATA:
      return Object.assign({}, state, {
        isLoading: true
      })
    case RECEIVE_STATEMENT_DATA:
      return Object.assign({}, state, {
        isLoading: false,
        statement: action.statement || []
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  bankDataState,
  bankReducer
});

export default rootReducer;
